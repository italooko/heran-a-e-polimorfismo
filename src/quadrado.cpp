#include "quadrado.hpp"

Quadrado::Quadrado() {
    set_tipo("Quadrado");
    set_base(20.0);
    set_altura(20.0);
}

Quadrado::Quadrado(float lado) {
    set_tipo("Quadrado");
    set_base(lado);
    set_altura(lado);
}

Quadrado::~Quadrado() {

}

float Quadrado::calcula_area() {
    return get_base() * get_altura();
}

float Quadrado::calcula_perimetro() {
	return  (2.0 * get_base()) + (2.0 * get_altura());
}
#include "circulo.hpp"
#include <cmath>

const double pi = 3.14159265358979323846;

Circulo::Circulo() {
    set_tipo("Circulo");
    set_base(5.0);
    set_altura(5.0);
}

Circulo::Circulo(float raio) {
    set_tipo("Circulo");
    set_base(raio);
    set_altura(raio);
}

Circulo::~Circulo() {

}

float Circulo::calcula_area() {
    return pi * pow(get_base(), 2);
}

float Circulo::calcula_perimetro() {
	return 2.0 * pi * get_base();
}
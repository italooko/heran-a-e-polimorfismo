#include "triangulo.hpp"
#include <iostream>
#include <math.h>

Triangulo::Triangulo() {
    set_tipo("Triangulo");
    set_base(15.0);
    set_altura(20.0);
}

Triangulo::Triangulo(float base, float altura) {
    set_tipo("Triangulo");
    set_base(base);
    set_altura(altura);
}

Triangulo::Triangulo(string tipo, float base, float altura) {
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}

Triangulo::~Triangulo() {

}

float Triangulo::calcula_area() {
    return get_base() * get_altura() / 2.0;
}

float Triangulo::calcula_perimetro() {
	return hypotf(get_base(), get_altura()) + get_base() + get_altura();
}
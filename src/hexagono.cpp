#include "hexagono.hpp"
#include <cmath>

Hexagono::Hexagono() {
    set_tipo("Hexagono");
    set_base(30.0);
    set_altura(30.0);
}

Hexagono::Hexagono(float lado) {
    set_tipo("Hexagono");
    set_base(lado);
    set_altura(lado);
}

Hexagono::~Hexagono() {

}

float Hexagono::calcula_area() {
    return (3.0 * pow(get_base(), 2) * sqrt(3)) / 2.0;
}

float Hexagono::calcula_perimetro() {
	return  6.0 * (get_base());
}
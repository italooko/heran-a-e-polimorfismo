#include "formageometrica.hpp"
#include "triangulo.hpp"
#include "quadrado.hpp"
#include "circulo.hpp"
#include "paralelogramo.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"

#include <list>
#include <vector>

int main() {
    // Exemplo com list

    list <FormaGeometrica *> lista_de_formas;
    
    lista_de_formas.push_back(new Triangulo());
    lista_de_formas.push_back(new Quadrado());
    lista_de_formas.push_back(new Circulo());
    lista_de_formas.push_back(new Paralelogramo());
    lista_de_formas.push_back(new Pentagono());
    lista_de_formas.push_back(new Hexagono());

    for (FormaGeometrica * forma: lista_de_formas) {
        forma->imprime_dados();
    }

    // Exemplo com vector
    vector <FormaGeometrica *> vetor_de_formas;

    vetor_de_formas.push_back(new Triangulo("Triangulo Retangulo", 3.0, 6.0));
    vetor_de_formas.push_back(new Quadrado(4.0));
    vetor_de_formas.push_back(new Circulo(50.0));
    vetor_de_formas.push_back(new Paralelogramo(17.0, 10.0));
    vetor_de_formas.push_back(new Pentagono(50.0, 100.0));
    vetor_de_formas.push_back(new Hexagono(60.0));

    for (FormaGeometrica * forma: vetor_de_formas) {
        forma->imprime_dados();
    }

    return 0;
} 
#include "pentagono.hpp"

Pentagono::Pentagono() {
    set_tipo("Pentagono");
    set_base(5.0);
    set_altura(15.0);
}

Pentagono::Pentagono(float apotema, float lado) {
    set_tipo("Pentagono");
    set_base(apotema);
    set_altura(lado);
}

Pentagono::~Pentagono() {

}

float Pentagono::calcula_area() {
    return calcula_perimetro() * get_base() / 2.0;
}

float Pentagono::calcula_perimetro() {
	return  5.0 * (get_base());
}
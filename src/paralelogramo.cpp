#include "paralelogramo.hpp"

Paralelogramo::Paralelogramo() {
    set_tipo("Paralelogramo");
    set_base(15.0);
    set_altura(10.0);
}

Paralelogramo::Paralelogramo(float base, float altura) {
    set_tipo("Paralelogramo");
    set_base(base);
    set_altura(altura);
}

Paralelogramo::~Paralelogramo() {

}

float Paralelogramo::calcula_area() {
    return get_base() * get_altura();
}

float Paralelogramo::calcula_perimetro() {
	return  2.0 * (get_base() + get_altura());
}